import {React, useState, useEffect, useRef, useMemo} from 'react';
import './App.css';
import { Canvas } from "react-three-fiber";
import { SphereGeometry, MeshStandardMaterial,TextureLoader } from "three";
import myNormalMapTexture  from './textures/Rain_normal-min.png'
import raincolorMap  from './textures/Rain_color_random.png'
import { OrbitControls,Circle,Stats } from '@react-three/drei';
import "./styles/TextSphere.css"
import MainPage from './components/main';
import Experience from './components/experience';
import Me from './components/me';
import Contact from './components/contact';
import styled from 'styled-components';

import {BiPlanet} from '@react-icons/all-files/bi/BiPlanet'
import {BiBrain} from '@react-icons/all-files/bi/BiBrain'
import {Plant} from "@phosphor-icons/react";
import Fade from 'react-reveal/Fade';
import CustomCursor from './components/customCursor';




const Container = styled.div`


height : 100vh;
scroll-snap-type: y mandatory;
sroll-behavior: smooth;
overflow-y: auto;
scrollbar-width: none;
&::-webkit-scrollbar{
  display:none
}
/* put on top */

position:relative;

`
function useIsInViewport(ref) {
  const [isIntersecting, setIsIntersecting] = useState(false);

  useEffect(() => {
    const observer = new IntersectionObserver(([entry]) =>
      setIsIntersecting(entry.isIntersecting),
    );

    observer.observe(ref.current);

    return () => {
      observer.disconnect();
    };
  }, [ref]);

  return isIntersecting;
}

function Dot1 (){
  
}

function App() {
  const initCol = "rgba(182, 157, 244, 0.47)"
  const initSize = "16px"
  const[mainCol, setMainCol]= useState("#b69df4")
  const[expCol, setExpCol]= useState(initCol)
  const[meCol, setMeCol]= useState(initCol)
  const[size1,setSize1]= useState("20px")
  const[size2,setSize2]= useState(initSize)
  const[size3,setSize3]= useState(initSize)

  const mainPageRef = useRef(null);
  const experiencePageRef = useRef(null);
  const mePageRef = useRef(null);
  const isMainInViewport = useIsInViewport(mainPageRef);
  const isExpInViewport = useIsInViewport(experiencePageRef);
  const isMeInViewport = useIsInViewport(mePageRef);

  //mobile:
  // true,false,false
  //true,true,false
  //false,true,true

  //web:
  //true,true,false
  //true,true,true
  //false,true,true
  console.log(isMainInViewport+","+isExpInViewport+","+isMeInViewport)

  useEffect(() => {
    if (window.innerWidth <= 768) {
      if(isMainInViewport&&!isExpInViewport){
        setMainCol("#b69df4")
        setSize1("20px")
        setExpCol(initCol)
        setMeCol(initCol)
        setSize2(initSize)
        setSize3(initSize)
       
      } 
      if(isMainInViewport&&isExpInViewport){
        setExpCol("#e4c6e1")
        setSize2("20px")
        setMainCol(initCol)
        setMeCol(initCol)
        setSize1(initSize)
        setSize3(initSize)
      }
  
      if(!isMainInViewport){
        setMeCol("#ffe9b1")
        setSize3("20px")
        setMainCol(initCol)
        setExpCol(initCol)
        setSize1(initSize)
        setSize2(initSize)
       
      }
    }
    // desktop:
    else{
    if(!isMeInViewport&&isMainInViewport&&isExpInViewport){
      setMainCol("#b69df4")
      setSize1("20px")
      setExpCol(initCol)
      setMeCol(initCol)
      setSize2(initSize)
      setSize3(initSize)
     
    } 
    if(isMeInViewport&&isMainInViewport&&isExpInViewport){
      setExpCol("#e4c6e1")
      setSize2("20px")
      setMainCol(initCol)
      setMeCol(initCol)
      setSize1(initSize)
      setSize3(initSize)
    }

    if(!isMainInViewport&&isMeInViewport&&isExpInViewport){
      setMeCol("#ffe9b1")
      setSize3("20px")
      setMainCol(initCol)
      setExpCol(initCol)
      setSize1(initSize)
      setSize2(initSize)
     
    }
  }

  }, [isMainInViewport, isExpInViewport, isMeInViewport]);

  return (
    <>
    <Container >
      <div id='main-nav'>
        <nav id='vertical-nav'>
            <a href="#main-section"><BiPlanet style={{color:`${mainCol}`, height:`${size1}`,width:`${size1}`}} className='dot-nav'/></a>
            <a href="#experience-section"><BiBrain style={{color:`${expCol}`, height:`${size2}`,width:`${size2}`}} className='dot-nav'></BiBrain></a>
            <a href="#about-me-section"><Plant style={{color:`${meCol}`, height:`${size3}`,width:`${size3}`}} className='dot-nav'></Plant></a>
        </nav>
      </div>
      
      <Fade><MainPage ref={mainPageRef}/></Fade>
      <Fade><Experience ref={experiencePageRef}/></Fade>
      <Fade><Me ref={mePageRef}/></Fade>
      <Fade><Contact/> </Fade>
    </Container>
   
    </>
  );
}

export default App;
