import { React, useRef, useEffect } from 'react';
import Fade from 'react-reveal/Fade';
import FirstProj from './firstProj';
import SecondProj from './secondProj';
import ThirdProj from './thirdProj';
import FourthProj from './fourthProj';
import CustomCursor from './customCursor';
import Contact from './contact';
import BackHome from './backHome';
const ProjectPage = () => {
    const dhc = useRef(null);
    const maze = useRef(null);
    const mobile = useRef(null);
    const angular = useRef(null);
    useEffect(() => {
        // Get the hash from the URL
        const hash = window.location.hash;
        console.log(hash)
        if (hash && dhc.current && hash==="#dhc") {
            // Scroll to the target div
            dhc.current.scrollIntoView({ behavior: 'smooth' });
        }
        if (hash && maze.current && hash==="#maze") {
            // Scroll to the target div
            maze.current.scrollIntoView({ behavior: 'smooth' });
        }
        if (hash && mobile.current && hash==="#mobile") {
            // Scroll to the target div
            mobile.current.scrollIntoView({ behavior: 'smooth' });
        }
        if (hash && angular.current && hash==="#angular") {
            // Scroll to the target div
            angular.current.scrollIntoView({ behavior: 'smooth' });
        }
    }, []);
    return (
<>
<BackHome></BackHome>
        <div className='project-page'>
            
            <div className='proj-page-w'>
                
                <div ref={maze} id='maze'>
                    
                        <FirstProj id='maze-algorithm' />
                    
                </div>
                <div ref={dhc} id='dhc'>
                    <Fade right>
                        <SecondProj />
                    </Fade>
                </div>
                <div  ref={mobile} id='mobile'>
                <Fade left>
                    <ThirdProj/>
                </Fade>
                </div>
                <div  ref={angular} id='angular'>
                <Fade right>
                   <FourthProj/>
                </Fade>
                </div>
            </div>
            
        </div>
        <Contact></Contact>
        </>
    )

};

export default ProjectPage;
