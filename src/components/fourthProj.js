import { React } from 'react';

const FourthProj = () => {
    return (
<div className='project4'>
                        <h3 className='dhc-proj-heading'>SproutSync<br/>
                            Plant Care Webpage</h3>
                        <div className='dhc-proj-content'>
                            <div className='dhc-proj-txt'>
                                <p className='react-proj-p'>Sprout Sync - a web tool that is still in development. 
                                Due to my interest in front-end development, I decided to deepen my knowledge of 
                                the Angular framework. I am doing so by working on a plant care web tool that aims to 
                                provide useful tips and tricks for the care of selected indoor plants.</p>
                                
                                <div className='proj-tags'>
                                    <div className='tag-skill-angular'>
                                        <p className='tag-angular'>Angular</p>
                                    </div>
                                    <div className='tag-skill-typescript'>
                                        <p className='tag-typescript'>TypeScript</p>
                                    </div>
                                    <div className='tag-skill-html-ss'>
                                        <p className='tag-html'>HTML</p>
                                    </div>
                                    <div className='tag-skill-java'>
                                        <p className='tag-p'>CSS</p>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div className='angular-proj-img'></div>
                            <a className='react-li' href="https://gitlab.com/vivienne_schoenenberger/sproutsync">View on gitlab</a>
                            </div>
                            
                        </div>
                    </div>
    )
}
export default FourthProj