import React, { useState, forwardRef } from 'react';
import styled from 'styled-components';
import { TextureLoader } from "three";
import myNormalMapTexture from '../textures/Rain_normal-min.png'
import raincolorMap from '../textures/Rain_color_random.png'
import { OrbitControls } from '@react-three/drei';
import Model from '../Scene';
import '../App.css';
import ReactLogo from '../images/react.png';
import Planet3 from '../images/planet_pink.png';
import Planet2 from '../images/planet2.svg'
import Planet4 from '../images/pixel_earth.png'
import Brain from '../images/brain_full.png'
import CloseDeepBlue from '../images/close-deep-blue.png'
import { Link } from 'react-router-dom';

import { ReactComponent as PlanetSub } from '../images/planet1.svg'



const Section = styled.div`

height : 100vh;
scroll-snap-align: center;
background-color:rgb(17, 12, 45);

display: flex;
align-items: center;
justify-content:center;
position:relative;
z-index:1;
overflow:hidden;
`
function Globe() {

  const normalMapTexture = new TextureLoader().load(myNormalMapTexture);
  const colorMap = new TextureLoader().load(raincolorMap);


  return (
    <>
      <ambientLight
        args={["#554391", 5]}
        intensity={0.1} />

      <directionalLight
        args={["#b69df4", 1]}
        position={[0, 5, 0]}
      />

      <directionalLight
        args={["#8E6FF2", 1]}
        position={[3, -3, -5]}
      />
      <directionalLight
        args={["#F5A590", 1]}
        position={[5, -3, 5]}
      />
      <directionalLight
        args={["#F5A590", 1]}
        position={[-5, 0, -3]}
      />

      <directionalLight
        args={["#ffe9b1", 1]}
        position={[0, -1, 0]}
      />
      <mesh position={[0, -2, 0]}>
        <Model />
        <meshStandardMaterial attach="material" color={"#623DB3"} />
        <OrbitControls
          enableZoom={false}
          enablePan={false}
          enableRotate={true}
          rotateSpeed={0.4}
          autoRotate={true}
        />
      </mesh>
    </>
  );
}

const onButtonClick = () => {
  // using Java Script method to get PDF file
  fetch('Vivienne_Schoenenberger_CV_2023.pdf').then(response => {
    response.blob().then(blob => {
      // Creating new object of PDF file
      const fileURL = window.URL.createObjectURL(blob);
      // Setting various property values
      let alink = document.createElement('a');
      alink.href = fileURL;
      alink.download = 'Vivienne_Schoenenberger_CV_2023.pdf';
      alink.click();
    })
  })
}



const Experience = forwardRef((props, ref) => {
  const [xCoordinate, setXCoordinate] = useState(null);
  const [yCoordinate, setYCoordinate] = useState(null);
  const [bookingToolShow, setBookingToolShow] = useState(false);
  const [mazeSolverShow, setMazeSolverShow] = useState(false);
  const [waterLooShow, setWaterLooShow] = useState(false);
  const [sproutSyncShow, setSproutSyncShow] = useState(false);
  const [hoverDiv, setHoverDiv] = useState(false)
  const [playStateMS, setPlayStateMS] = useState('')
  const [playStateSS, setPlayStateSS] = useState('')

  const [playStateWL, setPlayStateWL] = useState('')
  const [playStateBT, setPlayStateBT] = useState('')



  const handleMouseEnter = (event, planetName) => {
    const { left, top } = event.target.getBoundingClientRect();
    setXCoordinate(left);
    setYCoordinate(top);
    if (planetName === "sun") {
      setBookingToolShow(true);
      setPlayStateBT('paused')

      setMazeSolverShow(false)
      setPlayStateMS('')
      setWaterLooShow(false)
      setPlayStateWL('')
      setSproutSyncShow(false)
      setPlayStateSS('')
    }
    if (planetName === "react") {
      setMazeSolverShow(true)
      setPlayStateMS('paused')

      setBookingToolShow(false);
      setPlayStateBT('')
      setWaterLooShow(false)
      setPlayStateWL('')
      setSproutSyncShow(false)
      setPlayStateSS('')
    }
    if (planetName === "earth") {
      setWaterLooShow(true)
      setPlayStateWL('paused')

      setBookingToolShow(false);
      setPlayStateBT('')
      setMazeSolverShow(false)
      setPlayStateMS('')
      setSproutSyncShow(false)
      setPlayStateSS('')

    }
    if (planetName === "moon") {
      setSproutSyncShow(true)
      setPlayStateSS('paused')

      setBookingToolShow(false);
      setPlayStateBT('')
      setMazeSolverShow(false)
      setPlayStateMS('')
      setWaterLooShow(false)
      setPlayStateWL('')
    }

  };

  const handleCancel = (e) => {

    setBookingToolShow(false);
    setMazeSolverShow(false);
    setWaterLooShow(false);
    setSproutSyncShow(false);
    setPlayStateBT('')
    setPlayStateWL('')
    setPlayStateSS('')
    setPlayStateMS('')

  };


  return (

    <Section ref={ref} id='experience-section'>

      <div className="experience">

        <div className="brain">
          <div className="logos">
            <div className="sunCircle"><img style={{ animationPlayState: `${playStateBT}` }} onMouseEnter={e => handleMouseEnter(e, "sun")} src={Planet2} alt="fireSpot" className="sunIcon"></img>
              {bookingToolShow && <div className='project-bookingtool' style={{ top: `${yCoordinate - 265}` + 'px', left: `${xCoordinate - 265}` + 'px' }}>
                <div className='project-div'>
                  <div className='project-close'><img src={CloseDeepBlue} className='project-close-btn' onClick={e => handleCancel(e)}></img></div>
                  <div className='bt-popup-img'></div>
                  <h3 className='project-heading-bt'>Booking Tool DHC</h3>
                  <p className='project-description-bt'>A Web booking tool for the Digital Health Center  </p>
                  <div className='link-dhc-div'>
                    <Link className="link-to-dhc" to="/projects#dhc">
                      View Project
                    </Link>
                  </div>
                </div>
              </div>}</div>

            <div className="moonCircle"><img style={{ animationPlayState: `${playStateSS}` }} onMouseEnter={e => handleMouseEnter(e, "moon")} src={Planet3} alt="fireSpot" className="moonLogo" />
              {waterLooShow && <div className='project-waterloo' style={{ top: `${yCoordinate - 150}` + 'px', left: `${xCoordinate - 150}` + 'px' }}>
                <div className='project-close'><img src={CloseDeepBlue} className='project-close-btn' onClick={e => handleCancel(e)}></img></div>
                <div className='wl-popup-img'></div>
                <div className='project-div'>
                  <h3 className='project-heading-bt'>WaterLoo</h3>
                  <p className='project-description-bt'>Fun little Android app using an API</p>
                  <div className='link-waterloo-div'>
                    <Link className="link-to-waterloo" to="/projects#mobile">
                      View Project
                    </Link>
                  </div>
                </div></div>}
            </div>
            <div className=""><img style={{ animationPlayState: `${playStateMS}` }} onMouseEnter={e => handleMouseEnter(e, "react")} src={ReactLogo} alt="fireSpot" className="reactLogo" />
              {mazeSolverShow && <div className='project-maze' style={{ top: `${yCoordinate - 450}` + 'px', left: `${xCoordinate - 450}` + 'px' }}>
                <div className='project-close'><img src={CloseDeepBlue} className='project-close-btn' onClick={e => handleCancel(e)}></img></div>
                <div className='ms-popup-img'></div>
                <div className='project-div'>
                  <h3 className='project-heading-bt'>Maze Solver</h3>
                  <p className='project-description-bt'>Implementation of an algorithm into an existing three.js </p>
                  <div className='link-maze-div'>
                    <Link className="link-to-maze" to="/projects#maze">
                      View Project
                    </Link>
                  </div>
                </div></div>}</div>

            <div className="earthCircle"><img style={{ animationPlayState: `${playStateWL}` }} onMouseEnter={e => handleMouseEnter(e, "earth")} src={Planet4} alt="fireSpot" className="earthlogo" />
              {sproutSyncShow && <div className='project-sproutsync' style={{ top: `${yCoordinate - 200}` + 'px', left: `${xCoordinate - 200}` + 'px' }}>
                <div className='project-close'><img className='project-close-btn' src={CloseDeepBlue} onClick={e => handleCancel(e)}></img></div>
                <div className='ss-popup-img'></div>
                <div className='project-div'>
                  <h3 className='project-heading-bt' >SproutSync</h3>
                  <p className='project-description-bt'>An angular web tool that is still in development </p>
                  <div className='link-ss-div'>
                    <Link className="link-to-ss" to="/projects#angular">
                      View Project
                    </Link>
                  </div>
                </div></div>}</div>

          </div>
          <div>
            <img src={Brain} alt="Brain" className="brainImg"></img>
            <div className='hover-txt'>
              <h3 className='h3-projects'>Projects</h3>
              <p className='p-projects'>Hover over planets</p>
            </div>
          </div>

        </div>

        <div className='experience-txt'>
          <div className='exp-txt-content' >
            <div>
              <div>
              <h1 className="title-subpage">My Skills & Experience</h1>
              <div className='planet-sub-div'>
                <div ><PlanetSub className='planet-sub' /></div>
                <p className='p-sub-title-subpage'>JAVA DEVELOPER WITH FRONTEND FOCUS</p></div>
                <p className='p-subpage'>I have strong skills in Java development and a keen interest in frontend work.
                In school, I learned React.js and got hands-on experience with the Spring framework.
                I also explored Three.js in my free time and became familiar with Vaadin for my final exam. <br />
              </p>
              </div>
              <div className='exp-bottom'> 
                <div className='project-btn-mobile'>
                  <a className='link-to-projects' href="/projects">
                    <button className='btn-projects'>Projects</button>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Section>

  )
})
export default Experience