import { React } from 'react';

const ThirdProj = () => {
    return (
        <div className='project3'>
        <h3 className='react-proj-heading'>Water-loo <br/> Mobile App for Android</h3>
        <div className='react-proj-content'>
            <div>
            <div className='mobile-proj-img'></div>
            <a className='react-li' href="https://gitlab.com/vivienne_schoenenberger/water-loo">View on gitlab</a>
            </div>
            
            <div className='react-proj-txt'>
                <p className='react-proj-p'>
                    I developed this fun little Android app, 
                    which locates the nearest restroom using an API, 
                    as part of an extracurricular course (ÜK). 
                    It represents my first experience in mobile app 
                    development and serves as the spark for my interest 
                    in this field, motivating me to explore further 
                    development in this area during my free time in the future</p>
                
                <div className='proj-tags'>
                    <div className='tag-skill-android'>
                        <p className='tag-android'>Android Studio</p>
                    </div>
                    <div className='tag-skill-java'>
                        <p className='tag-p'>Java</p>
                    </div>

                </div>
            </div>
        </div>
    </div>
    )
}
export default ThirdProj