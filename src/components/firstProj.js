import { React } from 'react';
import Fade from 'react-reveal/Fade';
const FirstProj = () => {
    return (
        <div className='project1'>
            <h1 className="title-subpage">Projects</h1>
                <div className='proj-sub-div'>
                    <p className='p-sub-title-subpage'>CRAFTING DIGITAL REALMS</p></div>
                    <Fade left>
                        <h3 className='react-proj-heading'>Maze Solver</h3>
                        <div className='react-proj-content'>
                            <div>
                            <div className='react-proj-img'></div>
                            <a className='react-li' href="https://gitlab.com/vivienne_schoenenberger/maze-solver-algorithm">View on gitlab</a>
                            </div>
                            <div className='react-proj-txt'>
                                <p className='react-proj-p'>As part of the module 411, 
                                'Designing and Applying Data Structures and Algorithms,' 
                                I made the decision to implement an algorithm into an existing external project. 
                                Given my preexisting interest in Three.js and my intention to delve deeper into it, 
                                this collaboration naturally emerged. The clever backtracking algorithm I contributed 
                                enables the maze to be solved at the press of a button and in no time.</p>
                                
                                <div className='proj-tags'>
                                    <div className='tag-skill-threejs'>
                                        <p className='tag-threejs'>Three.js</p>
                                    </div>
                                    <div className='tag-skill-2'>
                                        <p className='tag-tween'>Tween.js</p>
                                    </div>
                                    <div className='tag-skill-backtracking'>
                                        <p className='tag-threejs'>Backtracking</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </Fade>
                    </div>
    )
}
export default FirstProj