import { React } from 'react';

const SecondProj = () => {
    return (
<div className='project2'>
                        <h3 className='dhc-proj-heading'>Workspace Booking Tool <br/>
                            For DHC</h3>
                        <div className='dhc-proj-content'>
                            <div className='dhc-proj-txt'>
                                <p className='react-proj-p'>
                                    The web booking tool for the Digital Health Center 
                                    (DHC) was developed as part of my Individual Practical Assignment (IPA) . 
                                    I utilized the Java-based Vaadin framework for its implementation, 
                                    leveraging Vaadin's component framework, Flow, 
                                    which greatly simplifies the creation of user interfaces (UIs).</p>
                                
                                <div className='proj-tags'>
                                    <div className='tag-skill-vaadin'>
                                        <p className='tag-p-vaadin'>Vaadin</p>
                                    </div>
                                    <div className='tag-skill-java'>
                                        <p className='tag-java'>Java</p>
                                    </div>
                                </div>
                            </div>
                            <div>
                            <div className='dhc-proj-img'></div>
                            <a className='react-li' href="https://gitlab.com/vivienne_schoenenberger/ipa-dhc-bookingtool">View on gitlab</a>
                            </div>
                            
                        </div>
                    </div>
    )
}
export default SecondProj