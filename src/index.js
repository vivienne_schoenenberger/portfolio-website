import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import { BrowserRouter , Route, Routes } from 'react-router-dom';
import reportWebVitals from './reportWebVitals';
import ProjectPage from './components/projects';
import FirstProj from './components/firstProj';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
<BrowserRouter>
      {/* Use the Routes component to define your routes */}
      <Routes>
        {/* Define individual routes using the Route component */}
        <Route path="/" element={<App />} />
        <Route path="/projects" element={<ProjectPage />} />

      </Routes>
    </BrowserRouter>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
